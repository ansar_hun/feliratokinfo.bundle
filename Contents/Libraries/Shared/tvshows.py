# coding=utf-8

import io
import os
import re
import shutil
import subprocess
import tempfile
from collections import namedtuple

from helper import BaseHelper


class TvShowHelper(BaseHelper):
    SubtitleFetchInfo = namedtuple('SubtitleFetchInfo', BaseHelper.SubtitleFetchInfo._fields + ('season', 'episode'))
    SUBTITLE_INFO_MATCHER = re.compile(r'.*\(((?P<TAG>[\w-]+)\.)*(?P<QUALITY>\w+)(-(?P<RELEASER>[\w-]+))?\)$')
    SUBTITLE_FILE_MATCHER = re.compile(r'S?(?P<season>\d+)([x_-]|\.)*E?(?P<episode>\d{2})', re.IGNORECASE)

    def getShowId(self, mediaId):
        params = {'action': 'tvdbidmapping', 'tvdbid': mediaId}
        searchResults = self.fetchJson(params)
        return searchResults['sid']

    def getSubtitleId(self, subtitle):
        return subtitle['felirat']

    def preProcessSubtitlesResultList(self, fetchInfo, searchResults):
        # With only one result the response will be a list
        if type(searchResults) is dict:
            searchResults = searchResults.values()

        searchResults = super(TvShowHelper, self).preProcessSubtitlesResultList(fetchInfo, searchResults)

        searchResults = filter(lambda x: int(x['ep']) == fetchInfo.episode or x['evadpakk'] == '1', searchResults)

        return searchResults

    def fetchSubtitles(self, fetchInfo, forceEpisodeFlag=False):
        # Note for episode filter use: 'rtol': fetchInfo.episode
        params = {'action': 'xbmc', 'sid': fetchInfo.id, 'ev': fetchInfo.season}

        # Note for stacked episodes without episode filter only one of the episode will be in the result list
        # In this case the subtitle is available with the episode filter
        if forceEpisodeFlag:
            params['rtol'] = fetchInfo.episode

        subtitle = self.getSubtitleInfo(params, fetchInfo)

        if subtitle:
            return subtitle
        elif not subtitle and not forceEpisodeFlag:
            # Maybe stacked episode, try again with episode filter
            return self.fetchSubtitles(fetchInfo, True)
        else:
            return None

    #  Archive handling
    def isArchive(self, fileName):
        return fileName.endswith('.zip') or fileName.endswith('.rar')

    def selectSubtitleFromArchive(self, fetchInfo, subtitles):
        # TODO: handle multiple match
        for subtitle in subtitles:
            for match in TvShowHelper.SUBTITLE_FILE_MATCHER.finditer(subtitle):
                fs = int(match.group('season'))
                fe = int(match.group('episode'))
                if fs == fetchInfo.season and fe == fetchInfo.episode:
                    return subtitle

    def handleArchive(self, content, fetchInfo):
        tmpDir = tempfile.mkdtemp()
        archivePath = tmpDir + '/archive'
        subtitleContent = None

        self.logger.Debug('Handling archive in %s', tmpDir)

        # shutil.copyfileobj(response, fd)
        with io.FileIO(archivePath, mode='wb') as f:
            f.write(content)

        process = subprocess.Popen("7z e %s" % archivePath, shell=True, cwd=tmpDir)
        process.wait()

        # TODO: handle directories
        files = os.listdir(tmpDir)
        subtitles = [f for f in files if f.endswith('.srt')]

        subtitle = self.selectSubtitleFromArchive(fetchInfo, subtitles)
        if subtitle:
            self.logger.Debug('Using subtitle from archive %s', subtitle)
            with io.FileIO(tmpDir + '/' + subtitle, mode='r') as f:
                subtitleContent = f.readall()

        shutil.rmtree(tmpDir)

        if not subtitleContent:
            self.logger.Warn('No matching subtitle found in the archive, subtitle files: %s', str(subtitles))

        return subtitleContent

    def downloadSubtitle(self, fetchInfo, subtitle):
        content = super(TvShowHelper, self).downloadSubtitle(fetchInfo, subtitle)

        if self.isArchive(subtitle['fnev']):
            content = self.handleArchive(content, fetchInfo)

        return content
