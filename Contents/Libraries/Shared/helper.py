import json
import urllib
import urllib2
import re
from collections import namedtuple


# The IPv6 address is not reachable for the site
def getIpv4AddressForSite():
    host = 'www.feliratok.info'
    try:
        import socket
        results = socket.getaddrinfo(host, 80)

        for result in results:
            if result[0] == socket.AF_INET:
                return result[4][0]
    except Exception:
        pass

    return host


class BaseHelper(object):
    BASE_URL = "http://%s/index.php" % getIpv4AddressForSite()
    LANGUAGES_MAP = {"None": "None", "Magyar": "hu", "Angol": "en"}
    SUBTITLE_INFO_MATCHER = re.compile(r'^((?P<TAG>[\w-]+)\.)*(?P<QUALITY>\w+)(-(?P<RELEASER>[\w-]+))?$')

    SubtitleFetchInfo = namedtuple('SubtitleFetchInfo', ['id', 'language', 'file'])

    def __init__(self, verbose, logger):
        self.verbose = verbose
        self.logger = logger

    # Public
    def getSubtitle(self, fetchInfo, downloadedSubtitleIds):
        subtitle = self.fetchSubtitles(fetchInfo)
        if subtitle:
            self.logger.Info('Subtitle found %s', json.dumps(subtitle))

            subtitleId = self.getSubtitleId(subtitle)
            languageCode = self.languageToCode(subtitle['language'])

            if subtitleId in downloadedSubtitleIds:
                self.logger.Debug('Subtitle already downloaded')
                content = None
            else:
                content = self.downloadSubtitle(fetchInfo, subtitle)

            return subtitleId, languageCode, content
        else:
            self.logger.Warn('No subtitle found for media')
            return None, None, None

    # Interface
    def fetchSubtitles(self, fetchInfo):
        pass

    def getSubtitleId(self, subtitle):
        pass

    def preProcessSubtitlesResultList(self, fetchInfo, searchResults):
        if not searchResults:
            searchResults = []

        searchResults = filter(lambda x: x['language'] == fetchInfo.language, searchResults)
        return searchResults

    # Protected
    #  Network
    def fetchUrl(self, params):
        url = "%s?%s" % (BaseHelper.BASE_URL, urllib.urlencode(params))
        request = urllib2.Request(url)
        import ssl
        return urllib2.urlopen(request, context=ssl._create_unverified_context())

    def fetchJson(self, params):
        response = self.fetchUrl(params)
        return json.load(response, 'utf-8')

    def downloadSubtitle(self, fetchInfo, subtitle):
        params = {'action': 'letolt', 'felirat': self.getSubtitleId(subtitle)}
        content = self.fetchUrl(params).read()

        return content

    #  Matcher
    def calculateScore(self, fetchInfo, result):
        scoreGroups = [
            {'id': 'TAG', 'match': 4, 'mismatch': -0.5},
            {'id': 'QUALITY', 'match': 2, 'mismatch': -0.5},
            {'id': 'RELEASER', 'match': 4, 'mismatch': -0.5},
        ]
        maxScore = 0

        name = result['nev']
        versions = name[name.rindex('(') + 1:name.rindex(')')].split(', ')

        for version in versions:
            score = 0

            match = BaseHelper.SUBTITLE_INFO_MATCHER.match(version)
            if not match:
                continue

            matchGroups = match.groupdict()

            for scoreGroup in scoreGroups:
                if matchGroups[scoreGroup['id']]:
                    if matchGroups[scoreGroup['id']].lower() in fetchInfo.file:
                        score += scoreGroup['match']
                    else:
                        score += scoreGroup['mismatch']

            if score > maxScore:
                maxScore = score

        result['_score_'] = maxScore

        return result

    def getSubtitleInfo(self, params, fetchInfo):
        try:
            searchResults = self.fetchJson(params)
        except ValueError as e:
            self.logger.Debug('Invalid json provided, maybe no subtitle for the media yet, %r', e)
            searchResults = []

        searchResults = self.preProcessSubtitlesResultList(fetchInfo, searchResults)
        searchResults = [self.calculateScore(fetchInfo, item) for item in searchResults]

        searchResults.sort(key=lambda x: (x['_score_']), reverse=True)

        if self.verbose:
            self.logger.Debug('Available subtitles %s', json.dumps(searchResults))

        if len(searchResults) > 0:
            return searchResults[0]
        else:
            return None

    #  Helper
    @staticmethod
    def languageToCode(language):
        return BaseHelper.LANGUAGES_MAP[language]
