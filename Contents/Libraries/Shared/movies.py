# coding=utf-8

from helper import BaseHelper


class MovieHelper(BaseHelper):
    def fetchSubtitles(self, fetchInfo):
        params = {'action': 'xbmcmovie', 'imdb': fetchInfo.id}
        return self.getSubtitleInfo(params, fetchInfo)

    def getSubtitleId(self, subtitle):
        return subtitle['id']
