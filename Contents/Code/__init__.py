# coding=utf-8

from movies import MovieHelper
from tvshows import TvShowHelper


def Start():
    pass


def addSubtitle(helper, fetchInfo, language, part):
    if not language:
        return

    Log.Info('Search subtitle for %s', fetchInfo)
    forceRedownload = Prefs['force_redownload']
    downloadedSubtitleIds = [] if forceRedownload else part.subtitles[helper.languageToCode(language)]
    subtitleId, languageCode, content = helper.getSubtitle(fetchInfo, downloadedSubtitleIds)
    if content:
        if forceRedownload:
            part.subtitles[languageCode].validate_keys([])

        part.subtitles[languageCode][subtitleId] = Proxy.Media(content, ext='srt')


class FeliratokInfoAgentTv(Agent.TV_Shows):
    name = 'Feliratok.info v2'
    languages = [Locale.Language.NoLanguage]
    primary_provider = False
    contributes_to = ['com.plexapp.agents.thetvdb']

    def search(self, results, media, lang, manual):
        title = media.primary_metadata.title
        mediaId = media.primary_metadata.id
        year = media.primary_metadata.originally_available_at.year

        Log.Info('Search show id for %s (%s), tvdb_id=%s', title, year, mediaId)
        helper = self.createHelper()
        showId = helper.getShowId(mediaId)
        Log.Info('Show id found %s', showId)

        results.Append(MetadataSearchResult(id=showId, score=100))

    def update(self, metadata, media, lang):
        helper = self.createHelper()

        for season in media.seasons:
            for episode in media.seasons[season].episodes:
                for item in media.seasons[season].episodes[episode].items:
                    for part in item.parts:
                        for language in (Prefs['lang1'], Prefs['lang2']):
                            fetchInfo = TvShowHelper.SubtitleFetchInfo(
                                id=metadata.id,
                                season=int(season),
                                episode=int(episode),
                                language=language,
                                file=part.file.lower()
                            )

                            addSubtitle(helper, fetchInfo, language, part)

    def createHelper(self):
        return TvShowHelper(Prefs['verbose'], Log)


class FeliratokInfoAgentMovie(Agent.Movies):
    name = 'Feliratok.info v2'
    languages = [Locale.Language.NoLanguage]
    primary_provider = False
    contributes_to = ['com.plexapp.agents.imdb']

    def search(self, results, media, lang, manual):
        title = media.primary_metadata.title
        mediaId = media.primary_metadata.id

        Log.Info('Retrieve id for %s, imdb_id=%s', title, mediaId)

        results.Append(MetadataSearchResult(id=mediaId, score=100))

    def update(self, metadata, media, lang):
        helper = self.createHelper()

        for item in media.items:
            for part in item.parts:
                for language in (Prefs['lang1'], Prefs['lang2']):
                    fetchInfo = MovieHelper.SubtitleFetchInfo(
                            id=metadata.id,
                            language=language,
                            file=part.file.lower()
                    )

                    addSubtitle(helper, fetchInfo, language, part)

    def createHelper(self):
        return MovieHelper(Prefs['verbose'], Log)
