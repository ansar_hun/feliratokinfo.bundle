import os
import sys

TEST_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_PATH = TEST_PATH + '/data/'
sys.path.append("%s/../Contents/Code" % TEST_PATH)
sys.path.append("%s/../Contents/Libraries/Shared" % TEST_PATH)

__builtins__['DATA_PATH'] = DATA_PATH
