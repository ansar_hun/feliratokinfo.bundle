class MockLogger(object):
    def Debug(self, *args):
        print 'LogDebug %r' % repr(args)

    def Info(self, *args):
        print 'LogInfo %r' % repr(args)

    def Warn(self, *args):
        print 'LogWarn %r' % repr(args)
