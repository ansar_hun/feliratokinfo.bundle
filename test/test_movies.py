import io
import os

import pytest

from movies import MovieHelper

from mocklogger import MockLogger


class MockMovieHelper(MovieHelper):
    def __init__(self):
        super(MockMovieHelper, self).__init__(False, MockLogger())

    def fetchUrl(self, params):
        filename = DATA_PATH + '/' + '_'.join("%s=%s" % (key, val) for (key, val) in params.iteritems()) + '.json'

        assert os.path.isfile(filename), 'No such resource \'%s\'' % filename

        return io.FileIO(filename, mode='r')


@pytest.mark.parametrize("imdbId,language,fileName,expectedName,expectedId", [
    ('tt2582496', 'Magyar', 'Me.and.Earl.and.the.Dying.Girl.2015.720p.WEB-DL.DD5.1.H264-RARBG.mkv', 'Me and Earl and the Dying Girl (2015) (BRRip-EVO, BRRip-ETRG, BDRip-GECKOS, 720p-YIFY, 720p-GECKOS, 1080p-YIFY, 1080p-GECKOS)', '1453453499'),
    ('tt2582496', 'Angol', 'Me.and.Earl.and.the.Dying.Girl.2015.720p.WEB-DL.DD5.1.H264-RARBG.mkv', 'Me and Earl and the Dying Girl (2015) (HDRip-ETRG, HDRip-EVO, WEBRip-RARBG, 720p-ETRG, WEB-DL.720p-EVO, WEB-DL.720p-RARBG, WEB-DL.720p-Ganool, WEB-DL.720p-MkvCage, WEB-DL.720p-PLAYNOW)', '1442689559'),
])
def test_fetchSubtitle(imdbId, language, fileName, expectedName, expectedId):
    agent = MockMovieHelper()

    fetchInfo = MockMovieHelper.SubtitleFetchInfo(
        id=imdbId,
        language=language,
        file=fileName.lower()
    )

    result = agent.fetchSubtitles(fetchInfo)
    assert result['nev'] == expectedName
    assert result['id'] == expectedId


def test_fetchSubtitleNonExisting():
    agent = MockMovieHelper()

    fetchInfo = MockMovieHelper.SubtitleFetchInfo(
        id='ttNONE',
        language='Magyar',
        file='NONE'
    )

    result = agent.fetchSubtitles(fetchInfo)
    assert result is None
