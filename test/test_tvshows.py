import io
import os

import pytest

from tvshows import TvShowHelper

from mocklogger import MockLogger


class MockTvShowHelper(TvShowHelper):
    def __init__(self):
        super(MockTvShowHelper, self).__init__(False, MockLogger())

    def fetchUrl(self, params):
        filename = DATA_PATH + '/' + '_'.join("%s=%s" % (key, val) for (key, val) in params.iteritems()) + '.json'

        assert os.path.isfile(filename), 'No such resource \'%s\'' % filename

        return io.FileIO(filename, mode='r')


def test_getShowId():
    agent = MockTvShowHelper()
    # The Walking Dead
    assert agent.getShowId(153021) == '1248'


def test_getShowIdNoMatch():
    agent = MockTvShowHelper()
    assert agent.getShowId(1530211) is None


@pytest.mark.parametrize("showId,season,episode,language,fileName,expectedName,expectedId", [
    # Season pack
    (1248, 5, 10, 'Magyar', 'The.Walking.Dead.S05E10.HDTV.x264-KILLERS.srt', 'The Walking Dead (Season 5) (HDTV)', '1429458872'),
    (1248, 5, 10, 'Angol', 'The.Walking.Dead.S05E10.720p.HDTV.x264-KILLERS.srt', 'The Walking Dead (Season 5) (720p)', '1429458825'),

    # Episodes
    #  Perfect match
    (1248, 6, 2, 'Angol', 'The.Walking.Dead.S06E02.720p.HDTV.x264-FLEET.srt', 'The Walking Dead - 6x02 (HDTV-FLEET)', '1445232913'),
    (1248, 6, 2, 'Magyar', 'The.Walking.Dead.S06E02.720p.HDTV.x264-FLEET.srt', 'The Walking Dead - 6x02 (HDTV-FLEET)', '1445247749'),
    #  Perfect match with tag
    (1248, 6, 1, 'Angol', 'The.Walking.Dead.S06E01.PROPER.720p.HDTV.x264-KiLLERS.srt', 'The Walking Dead - 6x01 (PROPER.720p-KiLLERS)', '1444633727'),
    #  Inperfect match with releaser
    (1248, 6, 3, 'Magyar', 'The.Walking.Dead.S06E03.720p.WEBRip.x264-FLEET.srt', 'The Walking Dead - 6x03 (WEBRip-Krissz)', '1445879068'),
    # Stacked episodes
    (3830, 1, 1, 'Magyar', 'The.Shannara.Chronicles.S01E01-E02.720p.HDTV.x264-KILLERS.mkv', 'The Shannara Chronicles - 1x02 (720p-KiLLERS)', '1452127442'),
])
def test_fetchSubtitle(showId, season, episode, language, fileName, expectedName, expectedId):
    agent = MockTvShowHelper()

    fetchInfo = TvShowHelper.SubtitleFetchInfo(
        id=showId,
        season=season,
        episode=episode,
        language=language,
        file=fileName.lower()
    )

    result = agent.fetchSubtitles(fetchInfo)
    assert result['nev'] == expectedName
    assert result['felirat'] == expectedId


def test_fetchSubtitleForNonExistingEpisode():
    agent = MockTvShowHelper()

    fetchInfo = TvShowHelper.SubtitleFetchInfo(
        id=3830,
        season=1,
        episode=10,
        language='Magyar',
        file='NOT_IMPORTANT'.lower()
    )

    assert agent.fetchSubtitles(fetchInfo) is None


@pytest.mark.parametrize("season,episode,language,fileName,expectedName", [
    # One result
    (5, 1, 'Magyar', 'The.Walking.Dead.S05E01.HDTV.x264-KILLERS.srt', 'The Walking Dead - 5x01 - No Sanctuary (HDTV-KILLERS, HDTV-AFG, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt'),
    (5, 10, 'Magyar', 'The.Walking.Dead.S05E10.HDTV.x264-KILLERS.srt', 'The Walking Dead - 5x10 - Them (HDTV-KILLERS, HDTV-AFG, HDTV-FUM, HDTV-iFT, 720p-KILLERS).hun.srt'),

    # Multiple result
    #  This should pick the second one
    (5, 3, 'Magyar', 'The.Walking.Dead.S05E03.HDTV.x264-ASAP.srt', 'The Walking Dead - 5x03 - Four Walls and a Roof (HDTV-AFG, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt'),
])
def test_selectSubtitleFromArchive(season, episode, language, fileName, expectedName):
    agent = MockTvShowHelper()

    files = (
        'The Walking Dead - 5x01 - No Sanctuary (HDTV-KILLERS, HDTV-AFG, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x02 - Strangers (HDTV-KILLERS, HDTV-AFG, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x03 - Four Walls and a Roof (HDTV-AFG, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x03 - Four Walls and a Roof (HDTV-ASAP).hun.srt',
        'The Walking Dead - 5x04 - Slabtown (HDTV-AFG, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x04 - Slabtown (HDTV-KILLERS).hun.srt',
        'The Walking Dead - 5x05 - Self Help (HDTV-KILLERS, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x06 - Consumed (HDTV-EVO, HDTV-FUM, 720p-DIMENSION).hun.srt',
        'The Walking Dead - 5x06 - Consumed (HDTV-KILLERS).hun.srt',
        'The Walking Dead - 5x07 - Crossed (HDTV-KILLERS, HDTV-EVO, HDTV-FUM, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x08 - Coda (REPACK.HDTV-KILLERS, HDTV-KILLERS, REPACK.HDTV-AFG, HDTV-AFG, REPACK.HDTV-EVO, HDTV-EVO, REPACK.HDTV-FUM, REPACK.720p-KILLERS, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x09 - What Happened and What\'s Going On (HDTV-KILLERS, HDTV-AFG, HDTV-FUM, HDTV-iFT, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x10 - Them (HDTV-KILLERS, HDTV-AFG, HDTV-FUM, HDTV-iFT, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x11 - The Distance (HDTV-ASAP, HDTV-AFG, HDTV-iFT, 720p-IMMERSE).hun.srt',
        'The Walking Dead - 5x11 - The Distance (INTERNAL.HDTV-BATV, INTERNAL.HDTV-AFG, INTERNAL.HDTV-FUM, INTERNAL.720p-BATV).hun.srt',
        'The Walking Dead - 5x12 - Remember (HDTV-KILLERS, HDTV-FUM, HDTV-iFT, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x13 - Forget (HDTV-KILLERS, HDTV-FUM, HDTV-iFT, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x14 - Spend (HDTV-KILLERS, HDTV-FUM, HDTV-iFT, 720p-KILLERS).hun.srt',
        'The Walking Dead - 5x15 - Try (HDTV-ASAP, HDTV-AFG, HDTV-iFT, 720p-IMMERSE).hun.srt',
        'The Walking Dead - 5x15 - Try (PROPER.HDTV-BATV, PROPER.HDTV-FUM, PROPER.720p-BATV).hun.srt',
        'The Walking Dead - 5x16 - Conquer (HDTV-ASAP, 720p-IMMERSE).hun.srt',
        'The Walking Dead - 5x16 - Conquer (PROPER.HDTV-KILLERS, REAL.PROPER.HDTV-AFG, REAL.PROPER.HDTV-FUM, HDTV-iFT, REAL.PROPER.720p-KILLERS).hun.srt',
    )

    fetchInfo = TvShowHelper.SubtitleFetchInfo(
        id=1248,
        season=season,
        episode=episode,
        language=language,
        file=fileName.lower()
    )

    result = agent.selectSubtitleFromArchive(fetchInfo, files)
    assert result == expectedName


def test_selectSubtitleFromArchiveWithoutSeparator():
    agent = MockTvShowHelper()

    files = (
        'marvels.agent.carter.201.hdtv-lol.srt',
    )

    fetchInfo = TvShowHelper.SubtitleFetchInfo(
        id=1111,
        season=2,
        episode=1,
        language='XX',
        file='Marvels.Agent.Carter.S02E01.720p.HDTV.X264-DIMENSION.mkv'.lower()
    )

    result = agent.selectSubtitleFromArchive(fetchInfo, files)
    assert result == 'marvels.agent.carter.201.hdtv-lol.srt'
