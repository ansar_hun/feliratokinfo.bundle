import hashlib

import pytest

from tvshows import TvShowHelper

from mocklogger import MockLogger


@pytest.mark.parametrize("tvdbId,expectedId", [
    ('281662', '3473'),  # Marvel's Daredevil
    ('153021', '1248'),  # The Walking Dead
    ('1530211', None),   # INVALID
])
def test_getShowId(tvdbId, expectedId):
    agent = TvShowHelper(False, MockLogger())

    id = agent.getShowId(tvdbId)
    assert id == expectedId


@pytest.mark.parametrize("season,episode,language,fileName,expectedId,expectedLang,expectedHash", [
    (5, 10, 'Magyar', 'The.Walking.Dead.S05E10.HDTV.x264-KILLERS.srt', '1429458872', 'hu', '5abe6c22d8a1aeb9babf90490cb46cf5'),
    (5, 10, 'Angol', 'The.Walking.Dead.S05E10.HDTV.x264-KILLERS.srt', '1429458825', 'en', '88c40f767eeaf2123c07b5ad48f02ab0'),
])
def test_downloadSubtitle(season, episode, language, fileName, expectedId, expectedLang, expectedHash):
    agent = TvShowHelper(False, MockLogger())

    fetchInfo = TvShowHelper.SubtitleFetchInfo(
        id=1248,
        season=season,
        episode=episode,
        language=language,
        file=fileName.lower()
    )

    id, language, content = agent.getSubtitle(fetchInfo, [])
    assert id == expectedId
    assert language == expectedLang
    assert hashlib.md5(content).hexdigest() == expectedHash
