import hashlib

import pytest

from movies import MovieHelper

from mocklogger import MockLogger

@pytest.mark.parametrize("imdb,language,fileName,expectedId,expectedLang,expectedHash", [
    ('tt3682448', 'Angol', 'Bridge.of.Spies.2015.720p.BluRay.x264-SPARKS.mkv', '1453026066', 'en', 'ee83f966fde6fb53a27c1fd4cc7b43dd'),
])
def test_downloadSubtitle(imdb, language, fileName, expectedId, expectedLang, expectedHash):
    agent = MovieHelper(True, MockLogger())

    fetchInfo = MovieHelper.SubtitleFetchInfo(
        id=imdb,
        language=language,
        file=fileName.lower()
    )

    id, language, content = agent.getSubtitle(fetchInfo, [])
    assert id == expectedId
    assert language == expectedLang
    assert hashlib.md5(content).hexdigest() == expectedHash
