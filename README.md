Plex agent for "feliratok.info" subtitles
=========================================

[![Build Status](https://drone.io/bitbucket.org/ansar_hun/feliratokinfo.bundle/status.png)](https://drone.io/bitbucket.org/ansar_hun/feliratokinfo.bundle/latest)

Not this is not a stable product.
**Use it on your own risk!**

How to use
----------

**Get the code**
```
#!bash

cd <PLEX-MEDIA-SERVER>/Plug-ins/
git clone https://bitbucket.org/ansar_hun/feliratokinfo.bundle.git
```

**Install the dependencies**

Install from the package manager:

* p7zip

Make sure the ```7z``` binary is in the ```$PATH```.

```
#!bash

sudo pacman -S p7zip                           # Arch linux
sudo apt-get install p7zip-full p7zip-rar      # Ubuntu
```

**Enable Agent**

Go to the server settings page (http://localhost:32400/web/index.html#!/settings/server).
Under ```Agents/TV Shows/TheTVDB``` enable the ```Feliratok.info v2``` agent and configure the downloadable subtitle languages.


Based on
--------

* https://github.com/balesz/SuperSubtitles.bundle
* https://github.com/fape/service.subtitles.supersubtitles
